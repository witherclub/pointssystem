package net.witherclub.pointssystem.commands;

import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.witherclub.pointssystem.PointsSystem;

public class CPointsSystem implements CommandExecutor {

	String message;

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		//Player player = (Player) sender;
		
		if (args.length >= 1) {
			switch(args[0].toLowerCase()) {
				
			case "give":
				if (args.length >= 3) {
					Player target = sender.getServer().getPlayer(args[1]);
					givePoints(target, Integer.parseInt(args[2]));
				}
				break;
			
			case "take":
				if (args.length >= 3) {
					Player target = sender.getServer().getPlayer(args[1]);
					takePoints(target, Integer.parseInt(args[2]));
				}
				break;
					
			default:
				message = "Usage: /pointssystem give/take <name> <amount>";
				break;
				
			}
		} else {
			message = "Usage: /pointssystem give/take <name> <amount>";
		}
		
		PointsSystem.logger.info(message);
		
		return true;
	}
	
	private void givePoints(Player player, int points) {
		
		if (player == null) {
			message = "That player is not online!";
			return;
		}
		
		UUID uuid = player.getUniqueId();
		String ign = player.getName();

		PointsSystem.logger.info("GIVE POINTS --- UUID: " + uuid + ", IGN: " + ign + ", POINTS: " + points );
		
		/**
		 * Database:
		 * UUID
		 * IGN
		 * Points
		**/
	}
	
	private void takePoints(Player player, int points) {
		
		if (player == null) {
			message = "That player is not online!";
			return;
		}
		
		UUID uuid = player.getUniqueId();
		String ign = player.getName();
		
		PointsSystem.logger.info("TAKE POINTS --- UUID: " + uuid + ", IGN: " + ign + ", POINTS: " + points );
		
		/**
		 * Database:
		 * UUID
		 * IGN
		 * Points
		**/
	}
}
