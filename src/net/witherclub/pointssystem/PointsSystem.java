package net.witherclub.pointssystem;

import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

import net.witherclub.pointssystem.commands.CPointsSystem;

public class PointsSystem extends JavaPlugin {
	
	public final static Logger logger = Logger.getLogger("Minecraft");
	
	@Override
	public void onEnable() {
		this.getCommand("pointssystem").setExecutor(new CPointsSystem());
	}
	
	@Override
	public void onDisable() {
		
	}
}